class User < ApplicationRecord
  include UserExtras

  has_one_attached :avatar

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
    :registerable,
    # :confirmable,
    # :recoverable,
    :rememberable,
    :invitable,
    :validatable

  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.email = auth.info.email
      user.password = Devise.friendly_token[0, 20]
      # user.name = auth.info.name   # assuming the user model has a name
      # user.image = auth.info.image # assuming the user model has an image
      # user.skip_confirmation!      # TODO: if devise confirmable
    end
  end

  def admin?
    id == 1
  end

  store_accessor :data, :first_name, :last_name
  store_accessor :data, :time_zone

  def full_name
    [first_name, last_name].join(" ")
  end
end
