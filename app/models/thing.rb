class Thing < ApplicationRecord
  include Discard::Model

  def self.where_json(key, value)
    where("json_extract(data, '$.#{key}') like '#{value}'")
  end
end
