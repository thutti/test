class Binary < Thing
  store_accessor :data, :filename
  has_one :byte_array, dependent: :destroy

  validates :filename, :byte_array, presence: true
end
