class Relation < ApplicationRecord
  belongs_to :having, polymorphic: true
  belongs_to :belonging, polymorphic: true
end
