class ApplicationController < ActionController::Base
  include Pundit::Authorization
  impersonates :user

  before_action :redirect_to_admin_create, if: -> { User.count.zero? }
  before_action :authenticate_user!, except: :welcome
  around_action :switch_time_zone, if: :current_user

  protect_from_forgery with: :exception

  rescue_from "Pundit::NotAuthorizedError" do |exception|
    render "not_authorized", status: :unauthorized
  end

  rescue_from ActiveRecord::RecordNotFound, with: :render_404

  private

  def redirect_to_admin_create
    redirect_to new_setup_path
  end

  def render_404
    render "application/404", status: "404"
  end

  def switch_time_zone(&block)
    Time.use_zone(current_user&.time_zone || "UTC", &block)
  end
end
