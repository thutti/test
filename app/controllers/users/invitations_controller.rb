class Users::InvitationsController < Devise::InvitationsController
  def create
    self.resource = invite_resource
    resource_invited = resource.errors.empty?

    yield resource if block_given?

    if resource_invited
      resource.update(invitation_sent_at: Time.zone.now) # This is required with skip_invitation
      render json: {url: accept_user_invitation_url(invitation_token: resource.raw_invitation_token)}
    else
      respond_with_navigational(resource) { render :new, status: :unprocessable_entity }
    end
  end

  private

  def invite_resource
    # Skip sending emails on invite
    # TODO: Make this configurable
    super { |user| user.skip_invitation = true }
  end
end
