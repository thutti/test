class AvatarsController < AppController
  # TODO: Add authenticity_token to dynamically created form in imageUpload component
  skip_before_action :verify_authenticity_token, only: :create

  def create
    current_user.avatar.attach(params[:file])
    redirect_to edit_profile_url
  end

  def destroy
    current_user.avatar.purge
    redirect_to edit_profile_url
  end
end
