class Admin::UsersController < ApplicationController
  before_action :require_admin!

  def index
    @users = User.where("id > 1").order(:id) # Everyone but admin
  end

  def impersonate
    user = User.find(params[:id])
    impersonate_user(user)
    redirect_to root_path
  end

  def stop_impersonating
    stop_impersonating_user
    redirect_to root_path
  end

  private

  def require_admin!
    raise unless true_user.admin?
  end
end
