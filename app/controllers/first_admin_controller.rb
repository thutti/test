class FirstAdminController < ApplicationController
  skip_before_action :redirect_to_admin_create, :authenticate_user!

  before_action do
    redirect_to(root_url) unless User.count.zero?
  end

  def new
    @user = User.new(id: 1)
  end

  def create
    # FIXME: Setting the id here breaks the next user registration as Postgres doesn't increase its id increment.
    # See https://stackoverflow.com/a/37972960/3449673
    @user = User.create(id: 1, confirmed_at: Time.zone.now, **user_params)
    redirect_to new_user_session_path, notice: "Admin user account created."
  end

  private

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
