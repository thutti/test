class ProfilesController < ApplicationController
  def edit
  end

  def update
    current_user.update(profile_params)
    redirect_to edit_profile_url, notice: "Profile updated."
  end

  private

  def profile_params
    params.require(:user).permit(:first_name, :last_name, :time_zone)
  end
end
