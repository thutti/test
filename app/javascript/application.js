import Alpine from "alpinejs"
import persist from "@alpinejs/persist"
import { clipboardCopy, confirm, emojiInput, imageUpload } from "alpine-components"

import mrujs from "mrujs"
import Cropper from "cropperjs"

Alpine.data("clipboardCopy", clipboardCopy)
Alpine.data("confirm", confirm)
Alpine.data("emojiInput", emojiInput)
Alpine.data("imageUpload", imageUpload)

window.Alpine = Alpine

Alpine.plugin(persist)

import "app"

Alpine.start()
mrujs.start()
