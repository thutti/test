import Cropper from "cropperjs"

export function clipboardCopy(text) {
  return {
    text: text,

    copyButton: {
      ["x-on:click"](e) {
        navigator.clipboard.writeText(this.text)
      },
    },
  }
}

export function emojiInput() {
  return {
    emoji: null,

    picker: {
      ["x-on:emoji-click"](e) {
        this.emoji = e.detail.emoji.unicode
      },
    },
  }
}

export function confirm() {
  return {
    modal: null,

    button: {
      ["x-on:click"](e) {
        e.preventDefault()

        const template = document.querySelector("#confirmModalTemplate")
        const clone = template.content.firstElementChild.cloneNode(true)
        clone.querySelector(".js-btn-yes").textContent = this.$el.textContent
        this.$root.append(clone)
        this.modal = new bootstrap.Modal(clone)
        this.modal.show()
      },
    },

    root: {
      ["x-on:confirmed"]() {
        this.modal.hide()
        this.$root.querySelector("form").submit()
      },
    },
  }
}

export function imageUpload() {
  return {
    cropper: null,
    active: false,

    imageInput: {
      ["x-on:change"](e) {
        e.preventDefault()

        if (e.target.files.length > 0) {
          var src = URL.createObjectURL(e.target.files[0])
          this.$refs.preview.src = src
          this.cropper = new Cropper(this.$refs.preview, {
            aspectRatio: 1,
          })
          this.active = true
        }
      },
    },

    uploadButton: {
      ["x-on:click"](e) {
        this.cropper.getCroppedCanvas().toBlob((blob) => {
          const formData = new FormData()
          formData.append("file", blob, "avatar.png")

          fetch("/avatar", {
            method: "POST",
            body: formData,
          }).then(() => {
            // Possible improvement: Instead of a full reload, we could update the UI client-side.
            location.reload()
          })
        })
      },
    },
  }
}
