module ApplicationHelper
  def engine_name
    ""
  end

  def confirm_to(text, href, options = {})
    content_tag(:div, {"x-data" => "confirm()", "x-bind" => "root"}) do
      button_to text, href, {"x-bind" => "button", **options}
    end
  end
end
