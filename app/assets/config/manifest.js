//= link application.css
//= link sprockets.js
//= link alpine-components.js

//= link app.js
//= link app/logo.svg
//= link app/logo_16.png
//= link app/logo_32.png
//= link app/logo_180.png
//= link app/logo_192.png
//= link app/logo_512.png
//= link app/logo_large.png

//= link_tree ../images

//= link_tree ../../javascript .js

//= link app_manifest.js
