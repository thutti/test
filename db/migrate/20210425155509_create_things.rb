class CreateThings < ActiveRecord::Migration[6.1]
  def change
    create_table :things do |t|
      t.json :data, null: false, default: {}
      t.string :type
      t.datetime :discarded_at

      t.timestamps
    end
    add_index :things, :data, using: :gin
    add_index :things, :discarded_at
  end
end
