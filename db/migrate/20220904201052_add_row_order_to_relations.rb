class AddRowOrderToRelations < ActiveRecord::Migration[7.0]
  def change
    add_column :relations, :row_order, :integer
  end
end
