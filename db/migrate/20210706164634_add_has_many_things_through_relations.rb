class AddHasManyThingsThroughRelations < ActiveRecord::Migration[6.1]
  def change
    create_table :relations do |t|
      t.string :type

      t.bigint :having_id
      t.string :having_type

      t.bigint :belonging_id
      t.string :belonging_type

      t.timestamps
    end
    add_index :relations, :data, using: :gin
    add_index :relations, [:having_type, :having_id]
    add_index :relations, [:belonging_type, :belonging_id]
  end
end
