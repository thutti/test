class CreateByteArray < ActiveRecord::Migration[6.1]
  def change
    create_table :byte_arrays do |t|
      t.binary :data, null: false
      t.integer :binary_id

      t.timestamps
    end
  end
end
