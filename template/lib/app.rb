require "app/engine"

module App
  def self.config
    OpenStruct.new(
      name: "My Awesome Application",
      short_name: "My App",
      description: "It's good.",

      # Set domain of your production environment.
      # Otherwise mailers raise errors.
      # domain: "example.com",

      # TODO: make these optional!
      theme_color: "hotpink",
      background_color: "#ffffff"
      # text_color: "#f7f7f7",

      # Define PWA shortcut actions here (manifest.json)
      # Example:
      #
      # shortcuts: [
      #   {
      #     name: "New task",
      #     url: "/tasks/new"
      #   }
      # ],

      # Define endpoint to "Share with..." your PWA (manifest.json)
      # share_target: {
      #   action: "/links/new",
      #   method: "GET",
      #   enctype: "application/x-www-form-urlencoded",
      #   params: {
      #     title: "title",
      #     url: "url"
      #   }
      # },
    )
  end
end
