require_relative "boot"

require "rails/all"

# Require the gems listed in the app's Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

# Manually require from ghost_train's gemspec
require "litestack"
require "puma"
require "sass-rails"
require "sprockets" if Rails.env.development?
require "devise"
require "devise_invitable"
require "importmap-rails"
require "motor-admin"
require "pundit"
require "pretender"
require "discard"
require "hamlit"
require "bootstrap"
require "bootstrap_form"
require "letter_opener" if Rails.env.development?
require "pry-rails" if Rails.env.development?
require "initials"

require "app"

module GhostRails
  class Application < Rails::Application
    config.load_defaults 7.0

    # Merge in app's javascript importmap
    initializer "app.importmap", before: "importmap" do |app|
      app.config.importmap.paths << App::Engine.root.join("config/importmap.rb")
    end

    # Log to the app's ./log folder
    logger = Logger.new("#{App::Engine.root}/log/#{Rails.env}.log")
    logger.formatter = config.log_formatter
    config.logger = ActiveSupport::TaggedLogging.new(logger)

    # Use litejob as queuing backend for Active Job (and separate queues per environment).
    config.active_job.queue_adapter = :litejob
    config.active_job.queue_name_prefix = Rails.env
  end
end
