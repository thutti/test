Rails.application.routes.draw do
  get "manifest.json" => "progressive#manifest"
  get "offline" => "progressive#offline"
  get "service-worker.js" => "progressive#service_worker"
  root "progressive#start"

  resource :setup, only: [:new, :create, :show], controller: "first_admin"

  devise_for :users, controllers: {
    invitations: "users/invitations",
    sessions: "users/sessions",
    omniauth_callbacks: "users/omniauth_callbacks"
  }

  resource :account, only: [:show, :update]
  resource :avatar, only: [:create, :destroy]
  resource :profile, only: [:edit, :update]

  # if Rails.env.development?
  #   mount Lookbook::Engine, at: "/lookbook"
  # end

  authenticated :user, lambda { |u| u.admin? } do
    mount Motor::Admin => "/admin"

    namespace :admin do
      # This currently uses the same namespace as motor-admin.
      # But it seems to work just fine.
      resources :users, only: [:index] do
        post :impersonate, on: :member
        post :stop_impersonating, on: :collection
      end
    end
  end
end
