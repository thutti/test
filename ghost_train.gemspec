# frozen_string_literal: true

version = File.read(File.expand_path("VERSION", __dir__)).strip

Gem::Specification.new do |s|
  s.name = "ghost_train"
  s.version = version
  s.summary = "All aboard!"
  s.description = "I could've just used a ghost train."
  s.authors = ["Thomas Hutterer"]
  s.email = "tohu@tuta.io"
  s.homepage = "https://rubygems.org/gems/ghost_train"
  s.license = "MIT"

  s.executables += ["gspec", "importmap"]
  s.files = Dir["lib/**/*"]

  s.metadata = {
    "bug_tracker_uri" => "https://github.com/thutterer/ghost_train/issues",
    "changelog_uri" => "https://github.com/thutterer/ghost_train/releases/tag/v#{version}",
    "documentation_uri" => "https://github.com/thutterer/ghost_train/tree/v#{version}/README.md",
    "source_code_uri" => "https://github.com/thutterer/ghost_train/tree/v#{version}"
  }

  s.add_runtime_dependency "rails", "~> 7.0"
  s.add_runtime_dependency "puma", "~> 6.1"
  s.add_runtime_dependency "litestack"
  s.add_runtime_dependency "bootsnap", "~> 1.4", ">= 1.4.2"
  s.add_runtime_dependency "sass-rails", "~> 6.0"
  s.add_runtime_dependency "devise", "~> 4.7"
  s.add_runtime_dependency "devise_invitable"
  s.add_runtime_dependency "motor-admin"
  s.add_runtime_dependency "pundit", "~> 2.1"
  s.add_runtime_dependency "pretender"
  s.add_runtime_dependency "discard", "~> 1.2"
  s.add_runtime_dependency "hamlit"
  s.add_runtime_dependency "bootstrap", "~> 5.2"
  s.add_runtime_dependency "bootstrap_form"
  s.add_runtime_dependency "letter_opener", "~> 1.7"
  s.add_runtime_dependency "image_processing", ">= 1.2"
  s.add_runtime_dependency "importmap-rails"
  s.add_runtime_dependency "standard"
  s.add_runtime_dependency "rspec-rails", "~> 5.0.0"
  s.add_runtime_dependency "shoulda-matchers", "~> 5.0"
  s.add_runtime_dependency "pry-rails"
  s.add_runtime_dependency "initials"
end
