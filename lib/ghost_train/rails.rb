require "rubygems"

root_dir = File.expand_path("../..", __dir__)
APP_PATH = File.expand_path("config/application", root_dir)

# Generators run in scope of the app engine.
if ARGV[0] == "g" || ARGV[0] == "generate"
  ENGINE_ROOT = Gem::Specification.find_by_name("app").gem_dir
  ENGINE_PATH = File.expand_path("lib/app/engine", ENGINE_ROOT)

  require "rails/all"
  require "rails/engine/commands"
# Other commands, like server or console run with the full Rails app.
else
  require_relative File.expand_path("config/boot", root_dir)
  require "rails/commands"
end
