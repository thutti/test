# Ghost train

> _All aboard!_

Have you ever noticed how short after the excitement of a `rails new` you always have to do the same basic things all over again? User auth, database setup, frontend assets, ...
The moment you create a new Rails app, you duplicate dozens of files you had already in all your other Rails apps. Or worse, re-invent the wheel yet another time.

_That's not very DRY, is it?_

Here comes the Ghost train. It's a zero-conf, minimal-maintenance, full-stack web application framework based on Ruby On Rails.

> **Project status:** This project including this README is still pre-release. There is still a ton to add, fix, and most importantly document. If something is unclear or not working as described, please open an issue so we can fix it.


## Installation

You'll only need `git` and `docker` to get started with the [install script](https://github.com/thutterer/ghost_train/blob/master/install).

> **Using a Mac or Windows machine?** This script was only tested on a Linux so far. It should just work on a Mac, but no idea about Windows. Let me know!

It will download a couple of files, create a git repo for you and then build your container image. This step will take a minute or two.

**Make sure you run the installer in an empty directory**:

```bash
curl https://raw.githubusercontent.com/thutterer/ghost_train/master/install | sh
```

Congratulations, your new application is ready! 🎉

### Features

It comes with these features out of the box:

- [Devise](https://github.com/heartcombo/devise) for user authentication.
- [Pundit](https://github.com/varvet/pundit) for user authorization.
- [Hamlit](https://github.com/k0kubun/hamlit) as high performance templating engine.
- ViewComponent
- Bootstrap
- [Ahoy](https://github.com/ankane/ahoy) and [Motor Admin](https://github.com/motor-admin/motor-admin-rails) for analytics and administration.
- PostgreSQL ready to go. Users, analytics, and a single table for all your JSONB data.
- and more ... pretender, lookbook

It ships with everything you need to just run `docker compose up` and have a working application with a postgres database.

### Concept

Where Rails itself aims for _[Convention over configuration](https://en.wikipedia.org/wiki/Convention_over_configuration)_, Ghost train aims to eliminate all configuration. This means:

- No database migrations. No custom schema. ActiveRecord is ready for devise, ahoy and all your `things`, using JSONB.
- Actually, all files that you would normally find in your application's root folder are packaged away in the ghost_train gem. Out of sight, out of mind. Yes, read-only Rails!
- All you have to do is write an awesome Rails [_engine_](https://guides.rubyonrails.org/engines.html)!
- As it always was, you can add additional gems into your `Gemfile` and have a custom `routes.rb`.
- Ready to deploy!

#### How is it different from any Rails starter template?

There are some nice templates available to take a new Rails application off the ground faster. But those are all just one-shot ruby scripts that set up a bunch of stuff, and you still end up with a few dozens files, even some more now, and again have to version and maintain them.

Ghost train on the other hand doesn't create any files for you. It keeps them away from you. All of Rails' config is out of your sight, and hands.

## Configuration

Most of the features just work, but some need some configuration.

### Bootstrap

Automatically uses your `lib/app.rb` theme color as `$primary`.
Configure Bootstrap options and variables in your `theme.scss`.

### Login screen

By default, it will use the partial that comes with devise. To customize your login screen, create a new view at `app/views/users/sessions/new.html.erb`.

### Nice 404 error page

Styled page out of the box. Add your own at `app/views/posts/404.html.haml`.

## Writing tests with rspec

`rspec-rails` is all setup and ready. You'll have to invoke it slightly different though:

```bash
bundle exec gspec spec
```

So instead of `rspec`, use the `gspec` command. Also for the time being, you have to specify the `spec` folder.
I'll try to remove the need for both these things before a 1.x release.

## License
The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
